astroid==1.5.3
Babel==2.5.1
dj-database-url==0.4.2
Django==1.11.6
django-debug-toolbar==1.8
django-filter==1.1.0
django-phonenumber-field==1.3.0
djangorestframework==3.6.4
djangorestframework-gis==0.11.2
gunicorn==19.7.1
isort==4.2.15
lazy-object-proxy==1.3.1
Markdown==2.6.9
mccabe==0.6.1
phonenumberslite==8.8.5
psycopg2==2.7.3.2
py==1.4.34
pylint==1.7.4
pytest==3.2.3
pytz==2017.3
six==1.11.0
sqlparse==0.2.4
whitenoise==3.3.1
wrapt==1.10.11
