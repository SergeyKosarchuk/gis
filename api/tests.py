from django.test import TestCase
from django.contrib.gis.geos import GEOSGeometry
from django.utils.six import BytesIO
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.parsers import JSONParser
from .models import Supplier, Area
from .serializers import SupplierSerializer, AreaSerializer


# Create your tests here.

class SerializersTest(TestCase):
    """This class defines the test suite for the serialization model."""

    def setUp(self):
        """ Prepare model objects for tests. """
        self.supplier = Supplier(name='test_supplier',
                                 email='test@test.com',
                                 phone='+79998456644',
                                 address='This is test address')
        self.supplier.save()

        self.area = Area(name='test_area',
                         supplier=self.supplier,
                         geometry='{"type": "Polygon", "coordinates": [[[100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0]]]}',
                         services={'test1': 100.00, 'test2': 200.00})
        self.area.save()

    def test_supplier_serializations(self):
        serialized_supplier = SupplierSerializer(self.supplier)
        self.assertEqual(self.supplier.name, serialized_supplier.data['name'])
        self.assertEqual(self.supplier.email, serialized_supplier.data['email'])
        self.assertEqual(self.supplier.phone, serialized_supplier.data['phone'])
        self.assertEqual(self.supplier.address, serialized_supplier.data['address'])

    def test_area_serializations(self):
        serialized_area = AreaSerializer(self.area)
        self.assertEqual(self.area.name, serialized_area.data['name'])
        self.assertEqual(self.area.supplier.id, serialized_area.data['supplier'])
        self.assertEqual(self.area.geometry, GEOSGeometry(str(serialized_area.data['geometry']), srid=4326))
        self.assertEqual(self.area.services['test1'], float(serialized_area.data['services']['test1']))

    def test_supplier_deserialization(self):
        json = b'''{
                    "id": 1,
                    "name": "test_supplier",
                    "email": "supplier1@server.com",
                    "address": "address",
                    "phone": "+79997128198" }'''
        stream = BytesIO(json)
        data = JSONParser().parse(stream)
        supplier = SupplierSerializer(data=data)
        # Supplier with this name already exists.
        self.assertEqual(supplier.is_valid(), False)
        # Change supplier name and try again.
        data['name'] = 'new_supplier'
        supplier = SupplierSerializer(data=data)
        self.assertEqual(supplier.is_valid(), True)

    def test_area_deserialization(self):
        json = b"""{
                    "id": 1,
                    "name": "test_area",
                    "supplier": 1,
                    "geometry": {
                        "type": "Polygon", 
                        "coordinates": [
                            [
                                [100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0]
                            ]
                        ]
                    },
                    "services": {
                        "test_service1": 100.0,
                        "test_service2": 200.0
                    }
                } """
        stream = BytesIO(json)
        data = JSONParser().parse(stream)
        area = AreaSerializer(data=data)
        # Area with this name already exists.
        self.assertEqual(area.is_valid(), False)
        # Change area name and try again.
        data['name'] = 'new_area'
        area = AreaSerializer(data=data)
        self.assertEqual(area.is_valid(), True)


class APITest(TestCase):
    """ Test API in general. """
    def setUp(self):
        """ Prepare test client. """
        self.client = APIClient()

    def tearDown(self):
        """  Logout from system. """
        pass

    def test_suppliers(self):
        """ Test get /suppliers/ """
        response = self.client.get('/suppliers/')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_areas(self):
        """ Test get /areas/ """
        response = self.client.get('/areas/')
        self.assertEquals(response.status_code, status.HTTP_200_OK)
