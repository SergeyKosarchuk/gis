from rest_framework import serializers
from rest_framework_gis.serializers import GeoModelSerializer
from .models import Supplier, Area




class SupplierSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Supplier
        fields = ('id', 'name', 'email', 'address', 'phone')


class AreaSerializer(GeoModelSerializer):
    services = serializers.DictField(child=serializers.DecimalField(
        max_digits=10, decimal_places=2))
    class Meta:
        model = Area
        geo_field = 'geometry'
        fields = ('id', 'name', 'supplier', 'geometry', 'services')
