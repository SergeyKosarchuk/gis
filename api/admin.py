from django.contrib import admin
from .models import Area, Supplier


# Register your models here.


admin.site.register(Area)
admin.site.register(Supplier)