from django.contrib.postgres.fields import HStoreField
from django.contrib.gis.db import models
from phonenumber_field.modelfields import PhoneNumberField


# Create your models here.

class Supplier(models.Model):
    name = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=30, unique=True)
    address = models.CharField(max_length=100)
    phone = PhoneNumberField()


    def __str__(self):
        return self.name

    # Metadata
    class Meta:
        ordering = ["name"]
        db_table = 'suppliers'



class Area(models.Model):
    name = models.CharField(max_length=50, unique=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    geometry = models.PolygonField(srid=4326)
    services = HStoreField()

    def __str__(self):
        return self.name

    # Metadata
    class Meta:
        ordering = ["name"]
        db_table = 'areas'
