from rest_framework import viewsets
from api.models import Area, Supplier
from api.serializers import SupplierSerializer, AreaSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import GEOSException

# Create your views here.


class SupplierViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Suppliers to be viewed or edited.
    """
    queryset = Supplier.objects.all().order_by('-name')
    serializer_class = SupplierSerializer


class AreaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Areas to be viewed or edited.
    """
    queryset = Area.objects.all().order_by('-name')
    serializer_class = AreaSerializer


@api_view()
def search(request):
    """
    API endpoint that allows Search API to get Suppliers by location and type of services.
    """
    type_ = request.query_params.get('type')
    latitude = request.query_params.get('latitude')
    longitude = request.query_params.get('longitude')
    if type_ and latitude and longitude is not None:
        try:
            # Cast latitude and longitude to Point object.
            point = GEOSGeometry(f'Point({latitude} {longitude})', srid=4326)
        except GEOSException:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # Main query for Search API.
        areas = Area.objects.values('supplier__name', 'name', 'services').filter(services__has_key=type_).filter(geometry__contains=point)
        return Response({areas})
    return Response(status=status.HTTP_400_BAD_REQUEST)
