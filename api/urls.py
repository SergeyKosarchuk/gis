from django.conf.urls import url, include
from rest_framework import routers
from . import views



router = routers.DefaultRouter()
router.register(r'suppliers', views.SupplierViewSet)
router.register(r'areas', views.AreaViewSet)



urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^search/', views.search),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]